import { ref, watch } from "vue";
import router from "../router";
import axios from "axios";

const Axios = axios.create({
  withCredentials: true,
  headers: {
    common: { "X-Requested-With": "XMLHttpRequest" },
  },
});
const reqInterceptor = Axios.interceptors.request.use((request) => {
  return request;
});

const resInterceptor = Axios.interceptors.response.use(undefined, function (error) {
  if (error.hasOwnProperty("response")) {
    if (error.response.status == 401 || error.response.status == 419) {
      router.replace({ name: "admin.auth.login" });
    } else if (error.response.status == 404) {
      router.replace({ name: "errors" });
    } else if (error.response.status == 403) {
      router.replace({
        name: "errors",
        params: { code: 403, message: "Access is unauthorized." },
      });
    }
    if ([401, 404, 403, 419].indexOf(error.response.status) >= 0) {
      console.clear();
    }
  }
  return Promise.reject(error);
});

watch(
  () => [reqInterceptor, resInterceptor],
  (newVal) => {
    axios.interceptors.request.eject(newVal[0]);
    axios.interceptors.response.eject(newVal[1]);
  }
);

export default function usePromise() {
  const result = ref<any>("");
  const errors = ref<any>("");
  const loading = ref<boolean>(false);
  const fetching = ref<boolean>(true);

  async function createPromise(url: string, args: { [key: string]: any } = {}) {
    try {
      Axios.defaults.baseURL = "https://event.spidersdev.link/api/v1/";
      if (args.hasOwnProperty("base")) {
        Axios.defaults.baseURL = "/";
        delete args["base"];
      }
      if (args.hasOwnProperty("wait")) {
        delete args["wait"];
      }
      if (!args.hasOwnProperty("loading") && !loading.value) {
        loading.value = true;
      }
      errors.value = "";
      // store.dispatch("setResponseAction")
      const response = await Axios(url, { ...args });
      result.value = response.data;
      if (!!result.value && result.value.hasOwnProperty("message")) {
        // store.dispatch("setResponseAction", {
        //     message: result.value.message,
        //     code: 200,
        //     status: "success",
        // })
      }
    } catch (e: any) {
      if (e.hasOwnProperty("response")) {
        if (e.response.status == 400) {
          errors.value = e.response.data.errors;
          // store.dispatch("setResponseAction", {
          //     message: "Invalid data given",
          //     code: 422,
          //     status: "warning",
          // })
        } else if (e.response.status == 500) {
          // store.dispatch("setResponseAction", {
          //     message: "Server error",
          //     code: 500,
          //     status: "error",
          // })
        }
      } else {
        console.log(e, e.response);
      }
    } finally {
      if (!args.hasOwnProperty("loading")) {
        fetching.value = false;
        loading.value = false;
      }
    }
  }

  return { result, errors, loading, fetching, createPromise };
}
