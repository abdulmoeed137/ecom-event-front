import type { App } from "vue";
import type { ModuleNamespace } from 'vite/types/hot';

/**
 * Register layouts in the app instance
 *
 * @param {App<Element>} app
 */

export function registerLayouts(app: App<Element>) {
    const layouts = import.meta.globEager<string, ModuleNamespace>('./*.vue');
    (<any>Object).entries(layouts).forEach(([, layout]: [number, any]) => {
        if (!!layout?.default?.name) {
            app.component(layout.default.name, layout.default);
        }
    });
}