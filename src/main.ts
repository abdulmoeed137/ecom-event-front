import { registerLayouts } from './layouts/register';
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { VDataTableServer } from "vuetify/labs/VDataTable"
import { createPinia } from 'pinia';
import { useMainStore } from './store';
import { fa } from "vuetify/iconsets/fa";
import { aliases, mdi } from "vuetify/lib/iconsets/mdi";
// make sure to also import the coresponding css
import "@mdi/font/css/materialdesignicons.css";

const vuetify = createVuetify({
    components: {
        ...components,
        VDataTableServer
    },
    directives,
    theme: {
        defaultTheme: "dark"
    }, icons: {
        defaultSet: "mdi",
        aliases,
        sets: {
            mdi,
            fa,
        },
    },
    defaults: {
        VDataTable: {
            fixedHeader: true,
            noDataText: "Results not found.",
        },
    }
})
const app = createApp(App)
app.use(createPinia()).use(router)
    .use(vuetify)
registerLayouts(app)
const store = useMainStore()
router.beforeEach(async (to, from, next) => {
    if (to.meta.hasOwnProperty("auth")) {
        if (to.meta.auth) {
            if (store.isAuth || await store.getUser()) {
                next();
            } else {
                next({ name: "admin.auth.login", replace: true });
            }
        } else {
            if (!store.isAuth && !await store.getUser()) {
                next();
            } else {
                next({ name: "admin.dashboard", replace: true });
            }
        }
    } else {
        next();
    }
})

app.mount('#app')
