import { createRouter, createWebHistory } from "vue-router"

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/admin",
      name: "admin.dashboard",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.,
      meta: {
        layout: "AdminLayout",
        auth: true
      },
      component: () => import("../pages/admin/Dashboard.vue")
    },
    {
      path: "/admin/login",
      name: "admin.auth.login",
      meta: {
        layout: "AdminLayout",
        auth: false
      },
      component: () => import("../pages/admin/auth/Login.vue")
    },
    {
      path: "/admin/edit/profile",
      name: "admin.edit.profile",
      meta: {
        layout: "AdminLayout",
        auth: true
      },
      component: () => import("../pages/admin/Edit.vue")
    },
    {
      path: "/admin/lists/news",
      name: "admin.lists.news",
      meta: {
        layout: "AdminLayout",
        auth: true
      },
      component: () => import("../pages/admin/lists/News.vue")
    },
    {
      path: "/admin/lists/pdfs",
      name: "admin.lists.pdfs",
      meta: {
        layout: "AdminLayout",
        auth: true
      },
      component: () => import("../pages/admin/lists/Pdfs.vue")
    },
    {
      path: "/admin/lists/partners",
      name: "admin.lists.partners",
      meta: {
        layout: "AdminLayout",
        auth: true
      },
      component: () => import("../pages/admin/lists/Partners.vue")
    },
    {
      path: "/admin/lists/tasks",
      name: "admin.lists.tasks",
      meta: {
        layout: "AdminLayout",
        auth: true
      },
      component: () => import("../pages/admin/lists/Tasks.vue")
    },
    {
      path: "/admin/lists/priorities",
      name: "admin.lists.priorities",
      meta: {
        layout: "AdminLayout",
        auth: true
      },
      component: () => import("../pages/admin/lists/Priorities.vue")
    },
    {
      path: "/admin/lists/years",
      name: "admin.lists.years",
      meta: {
        layout: "AdminLayout",
        auth: true
      },
      component: () => import("../pages/admin/lists/Years.vue")
    },
    {
      path: "/admin/lists/newsletters",
      name: "admin.lists.newsletters",
      meta: {
        layout: "AdminLayout",
        auth: true
      },
      component: () => import("../pages/admin/lists/Newsletters.vue")
    },
    {
      path: "/",
      name: "home",
      meta: {
        layout: "FrontLayout",
      },
      component: () => import("../pages/front/Home.vue")
    },
    {
      path: "/priorities",
      name: "priorities",
      meta: {
        layout: "FrontLayout",
      },
      component: () => import("../pages/front/Priorities.vue")
    },
    {
      path: "/periodical",
      name: "periodical",
      meta: {
        layout: "FrontLayout",
      },
      component: () => import("../pages/front/Periodical.vue")
    },
    {
      path: "/newsletters/:year(\\d+)",
      name: "newsletters",
      meta: {
        layout: "FrontLayout",
      },
      component: () => import("../pages/front/Newsletters.vue")
    },
    {
      path: "/bulletins",
      name: "bulletins",
      meta: {
        layout: "FrontLayout",
      },
      component: () => import("../pages/front/Bulletins.vue")
    },
    {
      path: "/highlights",
      name: "highlights",
      meta: {
        layout: "FrontLayout",
      },
      component: () => import("../pages/front/Highlights.vue")
    },
    {
      path: "/majlis",
      name: "majlis",
      meta: {
        layout: "FrontLayout",
      },
      component: () => import("../pages/front/Majlis.vue")
    },
    {
      path: "/news",
      name: "news.list",
      meta: {
        layout: "FrontLayout",
      },
      component: () => import("../pages/front/news/List.vue")
    },
    {
      path: "/news/:news_id(\\d+)",
      name: "news.single",
      meta: {
        layout: "FrontLayout",
      },
      component: () => import("../pages/front/news/Single.vue")
    },
    {
      path: "/:pathMatch(.*)*",
      component: () => import("../pages/Error.vue"),
      props: true,
      beforeEnter(to, from, next) {
        next({ replace: true })
      }
    },

  ]
})

export default router
