import type { UserDao } from "@/types/Dao/UserDao";
import usePromise from "../hooks/usePromise";
import { defineStore } from "pinia"
import type { MsgResponse } from "@/types";
interface State {
    user: UserDao | ""
    response: MsgResponse | "",
    loading: boolean
}
export const useMainStore = defineStore("mainStore", {
    state: (): State => ({
        user: "",
        response: "",
        loading: false
    }),
    getters: {
        isAuth: (state): boolean => !!state.user
    },
    actions: {
        setUser(user: UserDao | "" = "") {
            this.user = user
        },
        updateUser(user: { name: string, email: string }) {
            if (!!this.user) {
                this.user.name = user.name
                this.user.email = user.email
            }
        },
        setResponseAction(response: MsgResponse | "" = "") {
            this.response = response
        },
        setLoading(loading: boolean = false) {
            this.loading = loading
        },
        async getUser(): Promise<boolean> {
            const { result, createPromise } = usePromise()
            await createPromise("/admin/user");
            if (!!result.value?.data?.user) {
                this.setUser(result.value.data.user)
            }
            return !!this.user
        },
        logoutUser() {
            this.setUser()
        },
    },
});
