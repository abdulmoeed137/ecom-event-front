export interface NewsDao {
    id: number
    image_Path: string
    title_En: string
    body_En: string
    title_Ar: string
    body_Ar: string
    date: Date
}

export type NewsDaoStatic = {
    title: string
    body: string
    image: string
    date: Date
}