import type { YearDao } from "./Year"

export interface NewsLetterDao {
    id: number
    image_Path: string
    pdf_Path: string
    title_En: string
    title_Ar: string
    year: string | YearDao
    year_Id?: number
}

export type NewsLetterDaoStatic = {
    image: string,
    pdf: string,
    title: string
}