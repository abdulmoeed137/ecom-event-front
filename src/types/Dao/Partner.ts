export interface PartnerDao {
    id: number
    image_Path: string
    url: string
}

export type PartnerDaoStatic = {
    image: string
    url: string
}