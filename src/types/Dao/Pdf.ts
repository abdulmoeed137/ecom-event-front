export interface PdfDao {
    id: number
    title_Ar: string
    title_En: string
    image_Path: string,
    pdf_Path: string
}

export type PdfDaoStatic = {
    title: string
    image: string
    pdf: string
}