export interface TaskAndPriorityDao {
    id: number
    image_Path: string
    content_En: string
    content_Ar: string
}

export type PriorityDaoStatic = {
    image: string
    content: string
}

export type TaskDaoStatic = {
    content: string
}