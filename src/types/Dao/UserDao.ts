export interface UserDao {
    id: number,
    name: string,
    email: string
}