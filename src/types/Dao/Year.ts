import type { NewsLetterDao, NewsLetterDaoStatic } from "./NewsLetter"

export interface YearDao {
    id: number
    title_Ar: string
    title_En: string
    year: string
    newsletters?: NewsLetterDao[]
}

export interface SimpleYearDao {
    id: number
    year: string
    newsletters?: NewsLetterDao[]
}

export type YearDaostatic = {
    title: string
    year: number
}

export type NewsLetterYearDaostatic = {
    [year: string]: NewsLetterDaoStatic[]
}