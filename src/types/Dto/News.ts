export interface NewsDto {
    image?: File[]
    title_en: string
    body_en: string
    title_ar: string
    body_ar: string
    date?: Date
}