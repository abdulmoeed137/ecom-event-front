export interface NewsLetterDto {
    image?: File[]
    pdf?: File[]
    title_en: string
    title_ar: string
    year_id?: number
}