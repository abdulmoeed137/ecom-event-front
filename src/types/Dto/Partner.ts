export interface PartnerDto {
    image: File[] | undefined
    url: string
}