export interface PdfDto {
    image?: File[]
    pdf?: File[]
    title_ar: string
    title_en: string
}