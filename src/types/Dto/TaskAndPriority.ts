export interface TaskAndPriorityDto {
    image?: File[]
    content_en: string
    content_ar: string
}