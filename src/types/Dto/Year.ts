export interface YearDto {
    title_ar: string
    title_en: string
    year: string
}