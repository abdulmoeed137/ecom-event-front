export type MsgResponse = {
    code: number,
    message: string,
    status: "success" | "warning" | "error"
}