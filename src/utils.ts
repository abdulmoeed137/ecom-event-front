const ucFirst = (str: string): string =>
  str.split(" ").map(part => part.charAt(0).toUpperCase() + part.slice(1)).join(" ");

const convertDateStr = (d: string | Date): string => new Date(d).toISOString().split("T")[0].split("-").reverse().join("-")

export {
  ucFirst,
  convertDateStr
}
